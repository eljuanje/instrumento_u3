﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmCompra : FrmBase
    {
        public FrmCompra()
        {
            InitializeComponent();
        }

        private void FrmCompra_Load(object sender, EventArgs e)
        {
            string cmd = "Select * FROM Usuarios where Id_Usuario=" + FrmLogin.Codigo;

            DataSet ds;

            ds = Utilidades.Ejecutar(cmd);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
        }

        public static int cont_fila = 0;
        public static double total;

        private void btnColocar_Click(object sender, EventArgs e)
        {
            if (Utilidades.ValidarFormulario(this, errorProvider1) == false)
            {
                bool existe = false;
                int num_fila = 0;

                if (cont_fila == 0)
                {
                    dgvCompra.Rows.Add(txtNoSerie.Text, txtNomMarca.Text, txtNomModelo.Text, txtNomChasis.Text, txtTipoConfiguracion.Text, txtPrecioAutobus.Text, txtCantidad.Text);
                    double importe = Convert.ToDouble(dgvCompra.Rows[cont_fila].Cells[5].Value) * Convert.ToDouble(dgvCompra.Rows[cont_fila].Cells[6].Value);
                    dgvCompra.Rows[cont_fila].Cells[7].Value = importe;

                    cont_fila++;
                }
                else
                {
                    

                    if (existe == true)
                    {
                        dgvCompra.Rows[num_fila].Cells[6].Value = (Convert.ToDouble(dgvCompra.Rows[num_fila].Cells[6].Value)).ToString();
                        double importe = Convert.ToDouble(dgvCompra.Rows[num_fila].Cells[5].Value) * Convert.ToDouble(dgvCompra.Rows[num_fila].Cells[6].Value);

                        dgvCompra.Rows[num_fila].Cells[7].Value = importe;
                    }
                    else
                    {
                        dgvCompra.Rows.Add(txtNoSerie.Text, txtNomMarca.Text, txtNomModelo.Text, txtNomChasis.Text, txtTipoConfiguracion.Text, txtPrecioAutobus.Text, txtCantidad.Text);
                        double importe = Convert.ToDouble(dgvCompra.Rows[cont_fila].Cells[5].Value) * Convert.ToDouble(dgvCompra.Rows[cont_fila].Cells[6].Value);
                        dgvCompra.Rows[cont_fila].Cells[7].Value = importe;

                        cont_fila++;
                    }
                }

                total = 0;

                foreach (DataGridViewRow Fila in dgvCompra.Rows)
                {
                    total += Convert.ToDouble(Fila.Cells[7].Value);
                }

                lblTotalAPagar.Text = "MXN " + total.ToString();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(cont_fila > 0)
            {
                total = total - (Convert.ToDouble(dgvCompra.Rows[dgvCompra.CurrentRow.Index].Cells[7].Value));
                lblTotalAPagar.Text = "RD$ " + total.ToString();

                dgvCompra.Rows.RemoveAt(dgvCompra.CurrentRow.Index);

                cont_fila--;
            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            FrmConsultasProductos VenConPro = new FrmConsultasProductos();
            VenConPro.ShowDialog();

            if (VenConPro.DialogResult == DialogResult.OK)
            {
                txtNoSerie.Text = VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[0].Value.ToString();
                txtNomMarca.Text= VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[1].Value.ToString();
                txtNomModelo.Text = VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[2].Value.ToString();
                txtNomChasis.Text = VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[3].Value.ToString();
                txtTipoConfiguracion.Text = VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[4].Value.ToString();
                txtPrecioAutobus.Text= VenConPro.dgvConsultas.Rows[VenConPro.dgvConsultas.CurrentRow.Index].Cells[5].Value.ToString();
                txtCantidad.Focus();
            }
        }

        private void btnCompar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Gracias Por Su Compra, Esperamos Que Haya Sido De Su Agrado Adquirir Las Unidades Mediante Este Programa, Que Ofrecemos Para Su Comodidad");
        }

        private void btnFactura_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Su Factura Esta Siendo Elaborada, Y Sera Inviada A Su Correo Electronico");
        }
    }
}
