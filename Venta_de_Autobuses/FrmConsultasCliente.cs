﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmConsultasCliente : FrmConsultas
    {
        public FrmConsultasCliente()
        {
            InitializeComponent();
        }

        private void FrmConsultasCliente_Load(object sender, EventArgs e)
        {
            dgvConsultas.DataSource = LlenarDataGV("Cliente").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConsultas.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;

                    string cmd = "Select * FROM Cliente WHERE Nom_Cli LIKE ('%" + txtConsultas.Text.Trim() + "%') ";

                    ds = Utilidades.Ejecutar(cmd);

                    dgvConsultas.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha Ocurrido Un Error " + error.Message);
                }
            }
        }
    }
}
