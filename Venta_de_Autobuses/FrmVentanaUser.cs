﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmVentanaUser : Form
    {
        public FrmVentanaUser()
        {
            InitializeComponent();
        }

        private void FrmVentanaUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FrmVentanaUser_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuarios WHERE Id_Usuario=" + FrmLogin.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblNomUs.Text = DS.Tables[0].Rows[0]["Nom_Usuario"].ToString();
            lblUs.Text = DS.Tables[0].Rows[0]["Account"].ToString();
            lblCodigo.Text = DS.Tables[0].Rows[0]["Id_Usuario"].ToString();

            string url = DS.Tables[0].Rows[0]["Foto"].ToString();

            pictureBox1.Image = Image.FromFile(url);
        }

        private void btnPaginaPrincipal_Click(object sender, EventArgs e)
        {
            FrmPaginaPrincipalUsuario VenPagPrinUs = new FrmPaginaPrincipalUsuario();
            this.Hide();
            VenPagPrinUs.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            FrmLogin VenLog = new FrmLogin();
            this.Hide();
            VenLog.Show();
        }
    }
}
