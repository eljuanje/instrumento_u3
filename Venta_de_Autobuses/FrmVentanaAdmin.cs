﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmVentanaAdmin : Form
    {
        public FrmVentanaAdmin()
        {
            InitializeComponent();
        }

        private void FrmVentanaAdmin_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuarios WHERE Id_Usuario=" + FrmLogin.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblNomAd.Text = DS.Tables[0].Rows[0]["Nom_Usuario"].ToString();
            lblUsAdmin.Text = DS.Tables[0].Rows[0]["Account"].ToString();
            lblCodigo.Text = DS.Tables[0].Rows[0]["Id_Usuario"].ToString();

            string url = DS.Tables[0].Rows[0]["Foto"].ToString();

            pictureBox1.Image = Image.FromFile(url);
        }

        private void FrmVentanaAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnPaginaPrincipal_Click(object sender, EventArgs e)
        {
            FrmPaginaPrincipalAdmin VenPagPrinAd = new FrmPaginaPrincipalAdmin();
            this.Hide();
            VenPagPrinAd.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdministrarUsuarios_Click(object sender, EventArgs e)
        {
            FrmClientesAdmin VenAddCli = new FrmClientesAdmin();
            this.Hide();
            VenAddCli.Show();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            FrmLogin VenLogin = new FrmLogin();
            this.Hide();
            VenLogin.Show();
        }
    }
}
