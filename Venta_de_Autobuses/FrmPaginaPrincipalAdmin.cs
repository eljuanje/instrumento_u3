﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venta_de_Autobuses
{
    public partial class FrmPaginaPrincipalAdmin : FrmBase
    {
        public FrmPaginaPrincipalAdmin()
        {
            InitializeComponent();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            FrmProductosAdmin VenProAdmin = new FrmProductosAdmin();
            this.Hide();
            VenProAdmin.Show();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            FrmClientesAdmin VenCliAdmin = new FrmClientesAdmin();
            this.Hide();
            VenCliAdmin.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FrmVentanaAdmin VenAd = new FrmVentanaAdmin();
            this.Close();
            VenAd.Show();
        }
    }
}
