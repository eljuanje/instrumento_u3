﻿namespace Venta_de_Autobuses
{
    partial class FrmClientesAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdCli = new System.Windows.Forms.Label();
            this.lblNomCli = new System.Windows.Forms.Label();
            this.txtIdCli = new System.Windows.Forms.TextBox();
            this.txtNomCli = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(255, 12);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(255, 54);
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(255, 99);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(255, 148);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(255, 193);
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblIdCli
            // 
            this.lblIdCli.AutoSize = true;
            this.lblIdCli.Location = new System.Drawing.Point(38, 22);
            this.lblIdCli.Name = "lblIdCli";
            this.lblIdCli.Size = new System.Drawing.Size(21, 13);
            this.lblIdCli.TabIndex = 7;
            this.lblIdCli.Text = "ID:";
            // 
            // lblNomCli
            // 
            this.lblNomCli.AutoSize = true;
            this.lblNomCli.Location = new System.Drawing.Point(12, 67);
            this.lblNomCli.Name = "lblNomCli";
            this.lblNomCli.Size = new System.Drawing.Size(47, 13);
            this.lblNomCli.TabIndex = 8;
            this.lblNomCli.Text = "Nombre:";
            // 
            // txtIdCli
            // 
            this.txtIdCli.Location = new System.Drawing.Point(65, 19);
            this.txtIdCli.Name = "txtIdCli";
            this.txtIdCli.Size = new System.Drawing.Size(100, 20);
            this.txtIdCli.TabIndex = 9;
            // 
            // txtNomCli
            // 
            this.txtNomCli.Location = new System.Drawing.Point(65, 64);
            this.txtNomCli.Name = "txtNomCli";
            this.txtNomCli.Size = new System.Drawing.Size(184, 20);
            this.txtNomCli.TabIndex = 10;
            // 
            // FrmClientesAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 229);
            this.Controls.Add(this.txtNomCli);
            this.Controls.Add(this.txtIdCli);
            this.Controls.Add(this.lblNomCli);
            this.Controls.Add(this.lblIdCli);
            this.Name = "FrmClientesAdmin";
            this.Text = "FrmClientesAdmin";
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnAgregar, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblIdCli, 0);
            this.Controls.SetChildIndex(this.lblNomCli, 0);
            this.Controls.SetChildIndex(this.txtIdCli, 0);
            this.Controls.SetChildIndex(this.txtNomCli, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdCli;
        private System.Windows.Forms.Label lblNomCli;
        private System.Windows.Forms.TextBox txtIdCli;
        private System.Windows.Forms.TextBox txtNomCli;
    }
}