﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmProductosAdmin : FrmProductos
    {
        public FrmProductosAdmin()
        {
            InitializeComponent();
        }

        public override bool Agregar()
        {
            try
            {
                string cmd = string.Format("EXEC ActualizaArticulos '{0}','{1}','{2}','{3}','{4}','{5}'", txtNoSerie.Text.Trim(), txtNomMarca.Text.Trim(), txtNomModelo.Text.Trim(), txtNomChasis.Text.Trim(), txtTipoConfiguracion.Text.Trim(), txtPrecioAutobus.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se Ha Agregado Correctamente!...");
                return true;

                txtNoSerie.Text = "0";
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha Ocurrido Un Error " + error.Message);
                return false;
            }
        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarArticulos '{0}'", txtNoSerie.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se Ha Eliminado Producto");
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha Ocurrido Un Error " + error.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmConsultasProductos VenConProAd = new FrmConsultasProductos();
            VenConProAd.Show();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FrmPaginaPrincipalAdmin VenPrinAd = new FrmPaginaPrincipalAdmin();
            VenPrinAd.Show();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtNoSerie.Text = "";
            txtNomMarca.Text = "";
            txtNomModelo.Text = "";
            txtNomChasis.Text = "";
            txtTipoConfiguracion.Text = "";
            txtPrecioAutobus.Text = "";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {

        }
    }
}
