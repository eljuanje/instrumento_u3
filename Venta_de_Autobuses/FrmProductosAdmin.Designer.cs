﻿namespace Venta_de_Autobuses
{
    partial class FrmProductosAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrecio = new System.Windows.Forms.Label();
            this.lblTipoConfiguracion = new System.Windows.Forms.Label();
            this.lblNomChasis = new System.Windows.Forms.Label();
            this.lblNomModelo = new System.Windows.Forms.Label();
            this.lblNomMarca = new System.Windows.Forms.Label();
            this.txtNomModelo = new MiLibreria.ErrorTxtBox();
            this.txtNomMarca = new MiLibreria.ErrorTxtBox();
            this.txtNomChasis = new MiLibreria.ErrorTxtBox();
            this.txtTipoConfiguracion = new MiLibreria.ErrorTxtBox();
            this.txtPrecioAutobus = new MiLibreria.ErrorTxtBox();
            this.lblNoSerie = new System.Windows.Forms.Label();
            this.txtNoSerie = new MiLibreria.ErrorTxtBox();
            this.SuspendLayout();
            // 
            // btnAgregar
            // 
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.Location = new System.Drawing.Point(50, 206);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(40, 13);
            this.lblPrecio.TabIndex = 32;
            this.lblPrecio.Text = "Precio:";
            // 
            // lblTipoConfiguracion
            // 
            this.lblTipoConfiguracion.AutoSize = true;
            this.lblTipoConfiguracion.Location = new System.Drawing.Point(12, 171);
            this.lblTipoConfiguracion.Name = "lblTipoConfiguracion";
            this.lblTipoConfiguracion.Size = new System.Drawing.Size(75, 13);
            this.lblTipoConfiguracion.TabIndex = 31;
            this.lblTipoConfiguracion.Text = "Configuración:";
            // 
            // lblNomChasis
            // 
            this.lblNomChasis.AutoSize = true;
            this.lblNomChasis.Location = new System.Drawing.Point(46, 133);
            this.lblNomChasis.Name = "lblNomChasis";
            this.lblNomChasis.Size = new System.Drawing.Size(41, 13);
            this.lblNomChasis.TabIndex = 30;
            this.lblNomChasis.Text = "Chasis:";
            // 
            // lblNomModelo
            // 
            this.lblNomModelo.AutoSize = true;
            this.lblNomModelo.Location = new System.Drawing.Point(42, 97);
            this.lblNomModelo.Name = "lblNomModelo";
            this.lblNomModelo.Size = new System.Drawing.Size(45, 13);
            this.lblNomModelo.TabIndex = 29;
            this.lblNomModelo.Text = "Modelo:";
            // 
            // lblNomMarca
            // 
            this.lblNomMarca.AutoSize = true;
            this.lblNomMarca.Location = new System.Drawing.Point(47, 60);
            this.lblNomMarca.Name = "lblNomMarca";
            this.lblNomMarca.Size = new System.Drawing.Size(40, 13);
            this.lblNomMarca.TabIndex = 28;
            this.lblNomMarca.Text = "Marca:";
            // 
            // txtNomModelo
            // 
            this.txtNomModelo.Location = new System.Drawing.Point(93, 94);
            this.txtNomModelo.Name = "txtNomModelo";
            this.txtNomModelo.Size = new System.Drawing.Size(191, 20);
            this.txtNomModelo.TabIndex = 34;
            this.txtNomModelo.Validar = true;
            // 
            // txtNomMarca
            // 
            this.txtNomMarca.Location = new System.Drawing.Point(93, 57);
            this.txtNomMarca.Name = "txtNomMarca";
            this.txtNomMarca.Size = new System.Drawing.Size(191, 20);
            this.txtNomMarca.TabIndex = 35;
            this.txtNomMarca.Validar = true;
            // 
            // txtNomChasis
            // 
            this.txtNomChasis.Location = new System.Drawing.Point(93, 130);
            this.txtNomChasis.Name = "txtNomChasis";
            this.txtNomChasis.Size = new System.Drawing.Size(191, 20);
            this.txtNomChasis.TabIndex = 36;
            this.txtNomChasis.Validar = true;
            // 
            // txtTipoConfiguracion
            // 
            this.txtTipoConfiguracion.Location = new System.Drawing.Point(93, 168);
            this.txtTipoConfiguracion.Name = "txtTipoConfiguracion";
            this.txtTipoConfiguracion.Size = new System.Drawing.Size(191, 20);
            this.txtTipoConfiguracion.TabIndex = 37;
            this.txtTipoConfiguracion.Validar = true;
            // 
            // txtPrecioAutobus
            // 
            this.txtPrecioAutobus.Location = new System.Drawing.Point(93, 202);
            this.txtPrecioAutobus.Name = "txtPrecioAutobus";
            this.txtPrecioAutobus.Size = new System.Drawing.Size(191, 20);
            this.txtPrecioAutobus.TabIndex = 38;
            this.txtPrecioAutobus.Validar = true;
            // 
            // lblNoSerie
            // 
            this.lblNoSerie.AutoSize = true;
            this.lblNoSerie.Location = new System.Drawing.Point(33, 19);
            this.lblNoSerie.Name = "lblNoSerie";
            this.lblNoSerie.Size = new System.Drawing.Size(54, 13);
            this.lblNoSerie.TabIndex = 27;
            this.lblNoSerie.Text = "No. Serie:";
            // 
            // txtNoSerie
            // 
            this.txtNoSerie.Location = new System.Drawing.Point(93, 16);
            this.txtNoSerie.Name = "txtNoSerie";
            this.txtNoSerie.Size = new System.Drawing.Size(191, 20);
            this.txtNoSerie.TabIndex = 33;
            this.txtNoSerie.Validar = true;
            // 
            // FrmProductosAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 231);
            this.Controls.Add(this.txtPrecioAutobus);
            this.Controls.Add(this.txtTipoConfiguracion);
            this.Controls.Add(this.txtNomChasis);
            this.Controls.Add(this.txtNomMarca);
            this.Controls.Add(this.txtNomModelo);
            this.Controls.Add(this.txtNoSerie);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.lblTipoConfiguracion);
            this.Controls.Add(this.lblNomChasis);
            this.Controls.Add(this.lblNomModelo);
            this.Controls.Add(this.lblNomMarca);
            this.Controls.Add(this.lblNoSerie);
            this.Name = "FrmProductosAdmin";
            this.Text = "FrmProductosAdmin";
            this.Controls.SetChildIndex(this.lblNoSerie, 0);
            this.Controls.SetChildIndex(this.lblNomMarca, 0);
            this.Controls.SetChildIndex(this.lblNomModelo, 0);
            this.Controls.SetChildIndex(this.lblNomChasis, 0);
            this.Controls.SetChildIndex(this.lblTipoConfiguracion, 0);
            this.Controls.SetChildIndex(this.lblPrecio, 0);
            this.Controls.SetChildIndex(this.txtNoSerie, 0);
            this.Controls.SetChildIndex(this.txtNomModelo, 0);
            this.Controls.SetChildIndex(this.txtNomMarca, 0);
            this.Controls.SetChildIndex(this.txtNomChasis, 0);
            this.Controls.SetChildIndex(this.txtTipoConfiguracion, 0);
            this.Controls.SetChildIndex(this.txtPrecioAutobus, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnAgregar, 0);
            this.Controls.SetChildIndex(this.btnConsultar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.Label lblTipoConfiguracion;
        private System.Windows.Forms.Label lblNomChasis;
        private System.Windows.Forms.Label lblNomModelo;
        private System.Windows.Forms.Label lblNomMarca;
        private MiLibreria.ErrorTxtBox txtNomModelo;
        private MiLibreria.ErrorTxtBox txtNomMarca;
        private MiLibreria.ErrorTxtBox txtNomChasis;
        private MiLibreria.ErrorTxtBox txtTipoConfiguracion;
        private MiLibreria.ErrorTxtBox txtPrecioAutobus;
        private System.Windows.Forms.Label lblNoSerie;
        private MiLibreria.ErrorTxtBox txtNoSerie;
    }
}