﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venta_de_Autobuses
{
    public partial class FrmPaginaPrincipalUsuario : Form
    {
        public FrmPaginaPrincipalUsuario()
        {
            InitializeComponent();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            FrmCompra VenUsCompra = new FrmCompra();
            VenUsCompra.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FrmVentanaUser VenUs = new FrmVentanaUser();
            this.Close();
            VenUs.Show();
        }
    }
}
