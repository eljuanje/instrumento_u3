﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Venta_de_Autobuses
{
    public partial class FrmClientesAdmin : FrmProductos
    {
        public FrmClientesAdmin()
        {
            InitializeComponent();
        }

        public override bool Agregar()
        {
            try
            {
                string cmd = string.Format("EXEC ActualizaClientes '{0}','{1}'",txtIdCli.Text.Trim(),txtNomCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se Ha Agregado Correctamente!...");
                return true;

                txtIdCli.Text = "0";
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha Ocurrido Un Error " + error.Message);
                return false;
            }
        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarClientes '{0}'", txtIdCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se Ha Eliminado Cliente");
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha Ocurrido Un Error " + error.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            FrmPaginaPrincipalAdmin VenPrinAd = new FrmPaginaPrincipalAdmin();
            VenPrinAd.Show();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            FrmConsultasCliente VenConCli = new FrmConsultasCliente();
            VenConCli.Show();
        }
    }
}
