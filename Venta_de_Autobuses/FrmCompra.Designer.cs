﻿namespace Venta_de_Autobuses
{
    partial class FrmCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvCompra = new System.Windows.Forms.DataGridView();
            this.ColNoSerie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_Marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_Chasis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo_Configuracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio_Autobus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_Adquirida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total_Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblTotalAPagar = new System.Windows.Forms.Label();
            this.lblNoSerie = new System.Windows.Forms.Label();
            this.lblNomMarca = new System.Windows.Forms.Label();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblConfiguracion = new System.Windows.Forms.Label();
            this.lblChasis = new System.Windows.Forms.Label();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.btnColocar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.txtNoSerie = new MiLibreria.ErrorTxtBox();
            this.txtNomMarca = new MiLibreria.ErrorTxtBox();
            this.txtNomModelo = new MiLibreria.ErrorTxtBox();
            this.txtNomChasis = new MiLibreria.ErrorTxtBox();
            this.txtTipoConfiguracion = new MiLibreria.ErrorTxtBox();
            this.txtPrecioAutobus = new MiLibreria.ErrorTxtBox();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.lblImporte = new System.Windows.Forms.Label();
            this.txtCantidad = new MiLibreria.ErrorTxtBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnCompar = new System.Windows.Forms.Button();
            this.btnFactura = new System.Windows.Forms.Button();
            this.txtNomCli = new MiLibreria.ErrorTxtBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(906, 240);
            this.btnSalir.Size = new System.Drawing.Size(116, 23);
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // dgvCompra
            // 
            this.dgvCompra.AllowUserToAddRows = false;
            this.dgvCompra.AllowUserToDeleteRows = false;
            this.dgvCompra.AllowUserToResizeColumns = false;
            this.dgvCompra.AllowUserToResizeRows = false;
            this.dgvCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompra.ColumnHeadersVisible = false;
            this.dgvCompra.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColNoSerie,
            this.Nom_Marca,
            this.Nom_Modelo,
            this.Nom_Chasis,
            this.Tipo_Configuracion,
            this.Precio_Autobus,
            this.Cantidad_Adquirida,
            this.Total_Importe});
            this.dgvCompra.Location = new System.Drawing.Point(12, 58);
            this.dgvCompra.Name = "dgvCompra";
            this.dgvCompra.ReadOnly = true;
            this.dgvCompra.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvCompra.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCompra.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvCompra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompra.Size = new System.Drawing.Size(888, 205);
            this.dgvCompra.TabIndex = 0;
            // 
            // ColNoSerie
            // 
            this.ColNoSerie.HeaderText = "No. Serie";
            this.ColNoSerie.Name = "ColNoSerie";
            this.ColNoSerie.ReadOnly = true;
            this.ColNoSerie.Width = 130;
            // 
            // Nom_Marca
            // 
            this.Nom_Marca.HeaderText = "Marca";
            this.Nom_Marca.Name = "Nom_Marca";
            this.Nom_Marca.ReadOnly = true;
            // 
            // Nom_Modelo
            // 
            this.Nom_Modelo.HeaderText = "Modelo";
            this.Nom_Modelo.Name = "Nom_Modelo";
            this.Nom_Modelo.ReadOnly = true;
            this.Nom_Modelo.Width = 140;
            // 
            // Nom_Chasis
            // 
            this.Nom_Chasis.HeaderText = "Chasis";
            this.Nom_Chasis.Name = "Nom_Chasis";
            this.Nom_Chasis.ReadOnly = true;
            this.Nom_Chasis.Width = 105;
            // 
            // Tipo_Configuracion
            // 
            this.Tipo_Configuracion.HeaderText = "Configuración";
            this.Tipo_Configuracion.Name = "Tipo_Configuracion";
            this.Tipo_Configuracion.ReadOnly = true;
            this.Tipo_Configuracion.Width = 120;
            // 
            // Precio_Autobus
            // 
            this.Precio_Autobus.HeaderText = "Precio";
            this.Precio_Autobus.Name = "Precio_Autobus";
            this.Precio_Autobus.ReadOnly = true;
            // 
            // Cantidad_Adquirida
            // 
            this.Cantidad_Adquirida.HeaderText = "Cantidad";
            this.Cantidad_Adquirida.Name = "Cantidad_Adquirida";
            this.Cantidad_Adquirida.ReadOnly = true;
            // 
            // Total_Importe
            // 
            this.Total_Importe.HeaderText = "Importe";
            this.Total_Importe.Name = "Total_Importe";
            this.Total_Importe.ReadOnly = true;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(742, 273);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(34, 13);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total:";
            // 
            // lblTotalAPagar
            // 
            this.lblTotalAPagar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTotalAPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAPagar.Location = new System.Drawing.Point(782, 266);
            this.lblTotalAPagar.Name = "lblTotalAPagar";
            this.lblTotalAPagar.Size = new System.Drawing.Size(118, 26);
            this.lblTotalAPagar.TabIndex = 2;
            this.lblTotalAPagar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNoSerie
            // 
            this.lblNoSerie.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblNoSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoSerie.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNoSerie.Location = new System.Drawing.Point(12, 38);
            this.lblNoSerie.Name = "lblNoSerie";
            this.lblNoSerie.Size = new System.Drawing.Size(117, 23);
            this.lblNoSerie.TabIndex = 3;
            this.lblNoSerie.Text = "No. Serie";
            this.lblNoSerie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomMarca
            // 
            this.lblNomMarca.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblNomMarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomMarca.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNomMarca.Location = new System.Drawing.Point(123, 38);
            this.lblNomMarca.Name = "lblNomMarca";
            this.lblNomMarca.Size = new System.Drawing.Size(115, 23);
            this.lblNomMarca.TabIndex = 4;
            this.lblNomMarca.Text = "Marca";
            this.lblNomMarca.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblModelo
            // 
            this.lblModelo.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblModelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModelo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblModelo.Location = new System.Drawing.Point(233, 38);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(143, 23);
            this.lblModelo.TabIndex = 5;
            this.lblModelo.Text = "Modelo";
            this.lblModelo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblConfiguracion
            // 
            this.lblConfiguracion.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblConfiguracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfiguracion.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblConfiguracion.Location = new System.Drawing.Point(496, 38);
            this.lblConfiguracion.Name = "lblConfiguracion";
            this.lblConfiguracion.Size = new System.Drawing.Size(104, 23);
            this.lblConfiguracion.TabIndex = 6;
            this.lblConfiguracion.Text = "Configuración";
            this.lblConfiguracion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblChasis
            // 
            this.lblChasis.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblChasis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChasis.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblChasis.Location = new System.Drawing.Point(371, 38);
            this.lblChasis.Name = "lblChasis";
            this.lblChasis.Size = new System.Drawing.Size(128, 23);
            this.lblChasis.TabIndex = 7;
            this.lblChasis.Text = "Chasis";
            this.lblChasis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPrecio
            // 
            this.lblPrecio.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPrecio.Location = new System.Drawing.Point(596, 38);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(104, 23);
            this.lblPrecio.TabIndex = 8;
            this.lblPrecio.Text = "Precio";
            this.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnColocar
            // 
            this.btnColocar.Location = new System.Drawing.Point(907, 82);
            this.btnColocar.Name = "btnColocar";
            this.btnColocar.Size = new System.Drawing.Size(116, 23);
            this.btnColocar.TabIndex = 9;
            this.btnColocar.Text = "Colocar";
            this.btnColocar.UseVisualStyleBackColor = true;
            this.btnColocar.Click += new System.EventHandler(this.btnColocar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(906, 201);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(116, 23);
            this.btnEliminar.TabIndex = 10;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.Location = new System.Drawing.Point(906, 38);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(116, 23);
            this.btnProductos.TabIndex = 12;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = true;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // txtNoSerie
            // 
            this.txtNoSerie.Location = new System.Drawing.Point(12, 12);
            this.txtNoSerie.Name = "txtNoSerie";
            this.txtNoSerie.Size = new System.Drawing.Size(117, 20);
            this.txtNoSerie.TabIndex = 15;
            this.txtNoSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNoSerie.Validar = false;
            // 
            // txtNomMarca
            // 
            this.txtNomMarca.Location = new System.Drawing.Point(135, 12);
            this.txtNomMarca.Name = "txtNomMarca";
            this.txtNomMarca.Size = new System.Drawing.Size(103, 20);
            this.txtNomMarca.TabIndex = 16;
            this.txtNomMarca.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNomMarca.Validar = false;
            // 
            // txtNomModelo
            // 
            this.txtNomModelo.Location = new System.Drawing.Point(244, 12);
            this.txtNomModelo.Name = "txtNomModelo";
            this.txtNomModelo.Size = new System.Drawing.Size(132, 20);
            this.txtNomModelo.TabIndex = 17;
            this.txtNomModelo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNomModelo.Validar = false;
            // 
            // txtNomChasis
            // 
            this.txtNomChasis.Location = new System.Drawing.Point(383, 12);
            this.txtNomChasis.Name = "txtNomChasis";
            this.txtNomChasis.Size = new System.Drawing.Size(105, 20);
            this.txtNomChasis.TabIndex = 18;
            this.txtNomChasis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNomChasis.Validar = false;
            // 
            // txtTipoConfiguracion
            // 
            this.txtTipoConfiguracion.Location = new System.Drawing.Point(494, 12);
            this.txtTipoConfiguracion.Name = "txtTipoConfiguracion";
            this.txtTipoConfiguracion.Size = new System.Drawing.Size(106, 20);
            this.txtTipoConfiguracion.TabIndex = 19;
            this.txtTipoConfiguracion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTipoConfiguracion.Validar = false;
            // 
            // txtPrecioAutobus
            // 
            this.txtPrecioAutobus.Location = new System.Drawing.Point(606, 12);
            this.txtPrecioAutobus.Name = "txtPrecioAutobus";
            this.txtPrecioAutobus.Size = new System.Drawing.Size(94, 20);
            this.txtPrecioAutobus.TabIndex = 20;
            this.txtPrecioAutobus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioAutobus.Validar = false;
            // 
            // lblCantidad
            // 
            this.lblCantidad.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCantidad.Location = new System.Drawing.Point(697, 38);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(104, 23);
            this.lblCantidad.TabIndex = 24;
            this.lblCantidad.Text = "Cantidad";
            this.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblImporte
            // 
            this.lblImporte.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblImporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImporte.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblImporte.Location = new System.Drawing.Point(796, 38);
            this.lblImporte.Name = "lblImporte";
            this.lblImporte.Size = new System.Drawing.Size(104, 23);
            this.lblImporte.TabIndex = 25;
            this.lblImporte.Text = "Importe";
            this.lblImporte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(706, 12);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(94, 20);
            this.txtCantidad.TabIndex = 26;
            this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCantidad.Validar = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnCompar
            // 
            this.btnCompar.Location = new System.Drawing.Point(907, 125);
            this.btnCompar.Name = "btnCompar";
            this.btnCompar.Size = new System.Drawing.Size(115, 23);
            this.btnCompar.TabIndex = 27;
            this.btnCompar.Text = "Comprar";
            this.btnCompar.UseVisualStyleBackColor = true;
            this.btnCompar.Click += new System.EventHandler(this.btnCompar_Click);
            // 
            // btnFactura
            // 
            this.btnFactura.Location = new System.Drawing.Point(907, 163);
            this.btnFactura.Name = "btnFactura";
            this.btnFactura.Size = new System.Drawing.Size(115, 23);
            this.btnFactura.TabIndex = 28;
            this.btnFactura.Text = "Facturar";
            this.btnFactura.UseVisualStyleBackColor = true;
            this.btnFactura.Click += new System.EventHandler(this.btnFactura_Click);
            // 
            // txtNomCli
            // 
            this.txtNomCli.Location = new System.Drawing.Point(906, 12);
            this.txtNomCli.Name = "txtNomCli";
            this.txtNomCli.Size = new System.Drawing.Size(116, 20);
            this.txtNomCli.TabIndex = 29;
            this.txtNomCli.Validar = false;
            // 
            // FrmCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 297);
            this.Controls.Add(this.txtNomCli);
            this.Controls.Add(this.btnFactura);
            this.Controls.Add(this.btnCompar);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.lblImporte);
            this.Controls.Add(this.lblCantidad);
            this.Controls.Add(this.txtPrecioAutobus);
            this.Controls.Add(this.txtTipoConfiguracion);
            this.Controls.Add(this.txtNomChasis);
            this.Controls.Add(this.txtNomModelo);
            this.Controls.Add(this.txtNomMarca);
            this.Controls.Add(this.txtNoSerie);
            this.Controls.Add(this.btnProductos);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnColocar);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.lblChasis);
            this.Controls.Add(this.lblConfiguracion);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblNomMarca);
            this.Controls.Add(this.lblNoSerie);
            this.Controls.Add(this.lblTotalAPagar);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.dgvCompra);
            this.Name = "FrmCompra";
            this.Text = "FrmCompra";
            this.Load += new System.EventHandler(this.FrmCompra_Load);
            this.Controls.SetChildIndex(this.dgvCompra, 0);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.Controls.SetChildIndex(this.lblTotalAPagar, 0);
            this.Controls.SetChildIndex(this.lblNoSerie, 0);
            this.Controls.SetChildIndex(this.lblNomMarca, 0);
            this.Controls.SetChildIndex(this.lblModelo, 0);
            this.Controls.SetChildIndex(this.lblConfiguracion, 0);
            this.Controls.SetChildIndex(this.lblChasis, 0);
            this.Controls.SetChildIndex(this.lblPrecio, 0);
            this.Controls.SetChildIndex(this.btnColocar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnProductos, 0);
            this.Controls.SetChildIndex(this.txtNoSerie, 0);
            this.Controls.SetChildIndex(this.txtNomMarca, 0);
            this.Controls.SetChildIndex(this.txtNomModelo, 0);
            this.Controls.SetChildIndex(this.txtNomChasis, 0);
            this.Controls.SetChildIndex(this.txtTipoConfiguracion, 0);
            this.Controls.SetChildIndex(this.txtPrecioAutobus, 0);
            this.Controls.SetChildIndex(this.lblCantidad, 0);
            this.Controls.SetChildIndex(this.lblImporte, 0);
            this.Controls.SetChildIndex(this.txtCantidad, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnCompar, 0);
            this.Controls.SetChildIndex(this.btnFactura, 0);
            this.Controls.SetChildIndex(this.txtNomCli, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCompra;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblTotalAPagar;
        private System.Windows.Forms.Label lblNoSerie;
        private System.Windows.Forms.Label lblNomMarca;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblConfiguracion;
        private System.Windows.Forms.Label lblChasis;
        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.Button btnColocar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnProductos;
        private MiLibreria.ErrorTxtBox txtNoSerie;
        private MiLibreria.ErrorTxtBox txtNomMarca;
        private MiLibreria.ErrorTxtBox txtNomModelo;
        private MiLibreria.ErrorTxtBox txtNomChasis;
        private MiLibreria.ErrorTxtBox txtTipoConfiguracion;
        private MiLibreria.ErrorTxtBox txtPrecioAutobus;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.Label lblImporte;
        private MiLibreria.ErrorTxtBox txtCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNoSerie;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_Marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_Chasis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_Configuracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio_Autobus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_Adquirida;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total_Importe;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnCompar;
        private System.Windows.Forms.Button btnFactura;
        private MiLibreria.ErrorTxtBox txtNomCli;
    }
}