USE [Venta_de_Autobuses]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 04/11/2019 21:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[Nom_Cli] [varchar](50) NULL,
	[Id_Cli] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Cli] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Articulo]    Script Date: 04/11/2019 21:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulo](
	[Nom_Marca] [varchar](50) NULL,
	[Nom_Modelo] [varchar](max) NULL,
	[Nom_Chasis] [nvarchar](50) NULL,
	[Tipo_Configuracion] [varchar](3) NULL,
	[Precio_Autobus] [float] NULL,
	[No_Serie] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[No_Serie] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/11/2019 21:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Nom_Usuario] [varchar](50) NULL,
	[Account] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Status_Admin] [bit] NULL,
	[Foto] [varchar](500) NULL,
	[Id_Usuario] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Usuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proveedores]    Script Date: 04/11/2019 21:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedores](
	[Nom_Proveedor] [varchar](50) NULL,
	[Id_Proveedor] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Proveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facturas]    Script Date: 04/11/2019 21:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facturas](
	[FechaFac] [date] NULL,
	[Id_Cli] [int] NULL,
	[NumFac] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumFac] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[EliminarProveedores]    Script Date: 04/11/2019 21:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarProveedores]

@Id_Proveedor int

as

delete from Proveedores where Id_Proveedor=@Id_Proveedor
GO
/****** Object:  StoredProcedure [dbo].[EliminarClientes]    Script Date: 04/11/2019 21:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarClientes]

@Id_Cli int

as

delete from Cliente where Id_Cli=@Id_Cli
GO
/****** Object:  StoredProcedure [dbo].[EliminarArticulos]    Script Date: 04/11/2019 21:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarArticulos]

@No_Serie int

as

delete from Articulo where No_Serie=@No_Serie
GO
/****** Object:  StoredProcedure [dbo].[ActualizaProveedores]    Script Date: 04/11/2019 21:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaProveedores]

@Id_Proveedor int, @Nom_Proveedor varchar(50)

as

--Actualiza Proveedores

if NOT EXISTS (SELECT Id_Proveedor FROM Proveedores where Id_Proveedor=@Id_Proveedor)
insert into Proveedor(Nom_Proveedor) values (@Nom_Proveedor)

else

update Proveedores set Nom_Proveedor=@Nom_Proveedor where Id_Proveedor=@Id_Proveedor
GO
/****** Object:  StoredProcedure [dbo].[ActualizaClientes]    Script Date: 04/11/2019 21:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaClientes]

@Id_Cli int, @Nom_Cli varchar(50)

as

--Actualiza Clientes

if NOT EXISTS (SELECT Id_Cli FROM Cliente where Id_cli=@Id_Cli)
insert into Cliente(Nom_Cli) values (@Nom_Cli)

else

update Cliente set Nom_Cli=@Nom_Cli
GO
/****** Object:  StoredProcedure [dbo].[ActualizaArticulos]    Script Date: 04/11/2019 21:38:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaArticulos]

@No_Serie int, @Nom_Marca varchar(50), @Nom_Modelo varchar(100), @Nom_Chasis varchar(50), @Tipo_Configuracion varchar(3), @Precio_Autobus float

as

--Actualiza Articulos

if NOT EXISTS (SELECT No_Serie FROM Articulo where No_Serie=@No_Serie)
insert into Articulo (Nom_Marca,Nom_Modelo,Nom_Chasis,Tipo_Configuracion,Precio_Autobus) values (@Nom_Marca,@Nom_Modelo,@Nom_Chasis,@Tipo_Configuracion,@Precio_Autobus)

else

update Articulo set Nom_Marca=@Nom_Marca, Nom_Modelo=@Nom_Modelo, Nom_Chasis=@Nom_Chasis, Tipo_Configuracion=@Tipo_Configuracion, Precio_Autobus=@Precio_Autobus where No_Serie=@No_Serie
GO
